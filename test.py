import unittest
from unittest.mock import MagicMock
from roundup2bitbucket import RoundupReader


class TestSequenceFunctions(unittest.TestCase):

    def test_issues(self):
        sql_data = [
            (
                '20150102143010.123',  # activity
                0,                     # actor
                0,                     # assigned
                '20150101143010.123',  # creation
                0,                     # creator
                2,                     # priority
                1,                     # status
                'the bug',             # title
                9,                     # pk
                10,                    # retired
            ),
        ]
        r = RoundupReader()
        r.priorities = ['critical', 'urgent', 'bug', 'feature', 'wish']
        r.statuses = ['unread', 'deferred']
        r.users = ['takis']
        r.c = MagicMock()
        r.c.execute = MagicMock(return_value=sql_data)

        r.convert_issues()

        self.assertEqual(
            r.data['issues'],
            [{'assignee': None,
              'component': None,
              'content': '',
              'content_updated_on': '2015-01-01T14:30:10.123000',
              'created_on': '2015-01-01T14:30:10.123000',
              'edited_on': None,
              'id': 9,
              'kind': 'bug',
              'milestone': 'M1',
              'priority': 'major',
              'reporter': 'takis',
              'status': 'on hold',
              'title': 'the bug',
              'updated_on': '2015-01-02T14:30:10.123000',
              'version': None,
              'voters': None,
              'watchers': None}]
        )

    def test_comments(self):
        sql_data = [
            (
                '20150102143010.123',  # activity
                0,                     # actor
                0,                     # author
                '',                    # content
                '20150101143010.123',  # creation
                0,                     # creator
                '20150101143010.123',  # date
                1,                     # inreplyto
                2,                     # messageid
                'the msg',             # summary
                3,                     # type
                4,                     # pk
                5,                     # retired
                6,                     # link_id
                7,                     # node_id
            ),
        ]
        r = RoundupReader()
        r.users = ['takis']
        r.input_path = 'test'

        r.c = MagicMock()
        r.c.execute = MagicMock(return_value=sql_data)

        r.convert_messages()

        self.assertEqual(
            r.data['comments'],
            [
                {'content': 'the msg',
                 'created_on': '2015-01-01T14:30:10.123000',
                 'id': 4,
                 'issue': 7,
                 'updated_on': '2015-01-02T14:30:10.123000',
                 'user': 'takis'}
            ]
        )

if __name__ == '__main__':
    unittest.main()
